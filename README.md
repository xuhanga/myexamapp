

## 介绍
    在线答题小博士小程序提供在线模拟考试，在线刷题，完全开源，从界面上相对简陋一些，如需优化版请联系开发者

## 如何体验

    微信小程序搜索：在线答题小博士


### 更新记录

[CHANGELOG](./CHANGELOG.md)


## 目前已完成功能
+ 答题
+ 答题分单题模式和列表模式
+ 查看分数
+ 查看答案
+ 错题提醒
+ 查看答题历史记录
+ 查看错题记录
+ 生成海报

## 联系

目前小程序已经通过审核，并且发布，大家可以体验下，在学习的过程中遇到问题，可以咨询我，微信号：jglxiao

![Image text]( https://s1.ax1x.com/2020/03/28/GAACKU.jpg)
![Image text]( https://s1.ax1x.com/2020/03/28/GAAe8x.jpg)
![Image text]( https://s1.ax1x.com/2020/03/28/GAAYGt.jpg)
![Image text]( https://s1.ax1x.com/2020/03/28/GAAaM8.jpg)
![Image text]( https://s1.ax1x.com/2020/03/28/GAARMT.jpg)
![Image text]( https://s1.ax1x.com/2020/03/28/GAkz80.jpg)
![Image text]( https://s1.ax1x.com/2020/03/30/GmlB6O.jpg)
![Image text]( https://s1.ax1x.com/2020/03/30/Gm1MEd.jpg)
![Image text]( https://s1.ax1x.com/2020/03/31/GMIP0I.jpg)
![Image text]( http://file.xiaomutong.com.cn/IMG_9982%2820200409-201318%29.jpg)







